﻿namespace CoreStuff
{
    public enum CharacterArmor
    {
        None,
        Flippers,
        Boots, 
        FlipFlops
    }
    public class CommonData
    {
        public static int CurrentChildCount { get; set; }
        public static CharacterArmor CurrentArmor { get; set; }
    }
}