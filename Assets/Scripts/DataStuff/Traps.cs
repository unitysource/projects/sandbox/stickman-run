﻿using System;
using System.Collections.Generic;
using CoreStuff;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "New Trap", menuName = "Data/Trap")]
    public class Traps : ScriptableObject
    {
        public Material[] colorMaterials;
        public Material[] armorMaterials;
        // public Material[] trapMaterials;

        public Dictionary<CharacterArmor, Material> Armors;

        private void Awake()
        {
            Armors = new Dictionary<CharacterArmor, Material>
            {
                {CharacterArmor.Flippers, armorMaterials[0]},
                {CharacterArmor.Boots, armorMaterials[1]},
                {CharacterArmor.FlipFlops, armorMaterials[2]}
            };
        }
    }
}