﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Material))]
public class ArchMaterialChanger : MonoBehaviour
{
    [SerializeField] private Material colorMaterial;

    private Material _defaultMaterial;
    private MeshRenderer _meshRenderer;
    [SerializeField] private float flashDelay = 1f;

    private IEnumerator Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _defaultMaterial = _meshRenderer.material;

        while (gameObject)
        {
            _meshRenderer.material = _meshRenderer.material == _defaultMaterial ?
                colorMaterial : _defaultMaterial;
                
            yield return new WaitForSeconds(flashDelay);
        }
    }
}