using System;
using Character;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ui
{
    public class CharacterMoveUi : MonoBehaviour, IBeginDragHandler, IDragHandler
    {
        public static event Action<Direction> OnChangeLine;

        public void OnBeginDrag(PointerEventData eventData) => 
            OnChangeLine?.Invoke(eventData.delta.x < 0 ? Direction.Left : Direction.Right);

        public void OnDrag(PointerEventData eventData)
        {
        }
    }
}