﻿using System;
using CoreStuff;
using UnityEngine;

namespace Ui
{
    public class MainUi : MonoBehaviour
    {
        public static event Action OnMove;

        public void StartGame()
        {
            if (!Core.IsMove)
                Core.IsMove = true;
            OnMove?.Invoke();
        }
    }
}