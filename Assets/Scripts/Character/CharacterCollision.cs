﻿using CoreStuff;
using Tools;
using UnityEngine;

namespace Character
{
    public class CharacterCollision : MonoBehaviour
    {
        [SerializeField] private Children children;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(Tags.ChildInRoad))
            {
                if(CommonData.CurrentChildCount >= children.MAXCount) return;
                children.AddChild();
                Destroy(other.gameObject, .1f);
            }
            else if (other.gameObject.CompareTag(Tags.Finish))
            {
                Time.timeScale = 0;
                Debug.Log("Finish");
            }
            // Arches
            else if (other.gameObject.CompareTag(Tags.Arch))
            {
                Time.timeScale = 0;
                Debug.Log("Finish");
            }
        }
    }
}