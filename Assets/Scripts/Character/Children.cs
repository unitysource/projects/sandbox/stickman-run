﻿using System.Collections.Generic;
using System.Linq;
using CoreStuff;
using UnityEngine;

namespace Character
{
    public class Children : MonoBehaviour
    {
        [SerializeField] private GameObject childPrefab;
        [SerializeField] private Transform childrenFolder;
        [SerializeField] private int columnSize = 3;
        [SerializeField] private int startCount;
        [Tooltip("Distance between two children")] [SerializeField]
        private float childrenDistance = 1;

        [SerializeField] private int maxCount = 10;
        public int MAXCount => maxCount;
        
        private float _xSum;
        private static readonly int Run = Animator.StringToHash("Run");
        private readonly List<GameObject> _children = new List<GameObject>(10);

        private void Start()
        {
            for (int i = 0; i < startCount; i++) AddChild();
        }

        public void AddChild()
        {
            CommonData.CurrentChildCount++;

            var newChild = Instantiate(childPrefab, childrenFolder);
            newChild.GetComponent<Animator>().SetTrigger(Run);
            _children.Add(newChild);
            
            SetPositionForChild();
        }

        private void SetPositionForChild()
        {
            int count = _children.Count;
            Vector3 pos = Vector3.zero;
            Debug.Log($"Count: {count}, Child position: {pos}");

            float zPos = 0;

            // Find z position for new child
            for (int j = 1; j <= columnSize; j++)
            {
                if (count % columnSize == j || count == j)
                {
                    zPos = childrenDistance * j;
                    break;
                }

                if (count % columnSize == 0)
                {
                    zPos = childrenDistance * columnSize;
                    break;
                }
            }

            // Move left all children when creating new column
            if (count % columnSize == 1 && count != 1)
            {
                for (var index = 0; index < count - 1; index++)
                    _children[index].transform.localPosition += Vector3.left * childrenDistance * 0.5f;

                for (int j = 0; j < columnSize; j++)
                    if (count % columnSize == j || count == j)
                        _xSum += childrenDistance * 0.5f;

                _children.Last().transform.localPosition = new Vector3(pos.x + _xSum, pos.y, pos.x - zPos);
            }
            else if (count > 1)
                _children.Last().transform.localPosition = new Vector3(_children[count - 2].transform.localPosition.x,
                    pos.y, pos.x - zPos);
            else if (count == 1)
                _children.Last().transform.localPosition = new Vector3(0,
                    pos.y, pos.x - zPos);
        }
    }
}