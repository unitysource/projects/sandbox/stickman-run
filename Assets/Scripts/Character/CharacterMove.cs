﻿using Ui;
using UnityEngine;

namespace Character
{
    public enum Direction
    {
        Left,
        Right
    }

    public class CharacterMove : MonoBehaviour
    {
        [SerializeField] private float offset;
        [SerializeField] private float moveSpeed;

        private Rigidbody _rb;
        private Animator _animator;

        private void OnEnable()
        {
            CharacterMoveUi.OnChangeLine += ChangeLine;
        }

        private void OnDestroy()
        {
            CharacterMoveUi.OnChangeLine -= ChangeLine;
        }

        private void Start()
        {
            _rb = GetComponent<Rigidbody>();
            // _animator = GetComponent<Animator>();
            // _animator.SetTrigger(Run);
        }

        private void ChangeLine(Direction direction)
        {
            var position = transform.position;
            Vector3 newPosition = position + (direction is Direction.Left ? Vector3.left : Vector3.right) * offset;
            const float delta = 0.5f;
            if (direction is Direction.Left && newPosition.x >= -offset - delta ||
                direction is Direction.Right && newPosition.x <= offset + delta)
            {
                transform.position = Vector3.MoveTowards(position, newPosition,
                    moveSpeed * Time.deltaTime);
            }
        }
    }
}