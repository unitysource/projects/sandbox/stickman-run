﻿using Ui;
using UnityEngine;

namespace Character
{
    public class CharacterMain : MonoBehaviour
    {
        private static readonly int Run = Animator.StringToHash("Run");
        private Animator _animator;

        private void Start()
        {
            MainUi.OnMove += () => _animator.SetTrigger(Run);
            _animator = GetComponent<Animator>();
        }
    }
}