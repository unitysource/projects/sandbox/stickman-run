﻿using System;
using Data;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Road
{
    // public enum ArchType
    // {
    //     Water,
    //     Acid,
    //     Sand
    // }

    public class TrapModule : MonoBehaviour
    {
        // [SerializeField] private ArchType archType;
        [SerializeField] private Traps traps;
        // [SerializeField] private MeshRenderer trapRenderer;
        [SerializeField] private MeshRenderer[] arches;

        [SerializeField] private Material _requiredMaterial;

        private void Start()
        {
            int randSide = Random.Range(0, 2);
            int randWrongArmor = Random.Range(0, traps.armorMaterials.Length);
            Material newMaterial = traps.armorMaterials[randWrongArmor];

            while (newMaterial == _requiredMaterial)
            {
                randWrongArmor = Random.Range(0, traps.armorMaterials.Length);
                newMaterial = traps.armorMaterials[randWrongArmor];
            }

            Material[] materials = {_requiredMaterial, traps.colorMaterials[0]};
            Material[] wrongMaterials = {newMaterial, traps.colorMaterials[1]};
            arches[randSide].materials = materials;
            // arches[randSide].GetComponent<Arch>().armorType = 
            arches[randSide == 0 ? 1 : 0].materials = wrongMaterials;
            // arches[randSide == 0 ? 1 : 0].GetComponent<Arch>().archType = archType;
        }
    }
}