﻿using System;
using CoreStuff;
using Tools;
using UnityEngine;

namespace Road
{
    public class PlatformMove : MonoBehaviour
    {
        public static event Action OnInstantiateRoad;

        private void FixedUpdate()
        {
            if(Core.IsMove)
                transform.position += Vector3.back * GenerationRoad.PlatformSpeed / 10f * Time.deltaTime;
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag(Tags.InstantiatePointForRoad))
            {
                OnInstantiateRoad?.Invoke();    
            }
        }
    }
}