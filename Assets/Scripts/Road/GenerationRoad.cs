﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Road
{
    public enum ModuleType
    {
        Block,
        Trap
    }
    
    public class GenerationRoad : MonoBehaviour
    {
        [SerializeField] private List<GameObject> trapModules;
        [SerializeField] private List<GameObject> blockModules;
        [SerializeField] private GameObject finishModule;
        [SerializeField] private Transform pointToInstantiate;
        [SerializeField] private int countOfBlockModules = 2;
        // [SerializeField] private int countOfModules = 5;
        public const float PlatformSpeed = 100f;

        private bool _isFinish;
        // private ModuleType _currentModuleType;

        private void OnEnable()
        {
            PlatformMove.OnInstantiateRoad += CreateRoad;
        }

        private void OnDisable()
        {
            PlatformMove.OnInstantiateRoad -= CreateRoad;
        }

        private void CreateRoad()
        {
            Debug.Log($"countOfModules: {countOfBlockModules}");
            
            // if (countOfModules <= 0)
            // {
            //     Debug.Log("Finish");
            //     return;
            // }

            int randomTrapModule = Random.Range(0, trapModules.Count);
            int randomBlockModule = Random.Range(0, blockModules.Count);
            GameObject newRoadInstance;

            int random = Random.Range(0, 2);
            if (countOfBlockModules <= 0 && _isFinish is false)
            {
                _isFinish = true;
                newRoadInstance = trapModules[randomTrapModule];
                // _currentModuleType = ModuleType.Trap;
            }
            else if(countOfBlockModules <= 0 && _isFinish is true)
            {
                newRoadInstance = finishModule;
            }
            else if (random == 0 && countOfBlockModules > 0)
            {
                newRoadInstance = blockModules[randomBlockModule];
                countOfBlockModules--;
                // _currentModuleType = ModuleType.Block;
            }
            else if (random != 0 && countOfBlockModules > 0)
            {
                newRoadInstance = trapModules[randomTrapModule];
                // _currentModuleType = ModuleType.Trap;
            }
            else
                throw new Exception("Whaaaaat?");

            Instantiate(newRoadInstance, pointToInstantiate.position + Vector3.forward * 19.1f,
                Quaternion.identity);
        }
    }
}