﻿using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] private GameObject objectToFollow;
    [SerializeField] private float speed = 2.0f;
    [SerializeField] private float yOffset=10f;

    private void FixedUpdate()
    {
        var pos = transform.position;
        var endPos = new Vector3(objectToFollow.transform.position.x, objectToFollow.transform.position.y+yOffset, objectToFollow.transform.position.z);
        pos.z = Mathf.Lerp(pos.z, endPos.z, speed);
        pos.x = Mathf.Lerp(pos.x, endPos.x, speed);

        transform.position = pos;
    }
}