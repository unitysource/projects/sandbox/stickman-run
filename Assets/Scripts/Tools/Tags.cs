﻿namespace Tools
{
    public static class Tags
    {
        public const string Player = "Player";
        public const string Child = "Child";
        public const string ChildInRoad = "ChildInRoad";
        public const string InstantiatePointForRoad = "InstantiatePointForRoad";
        public const string Finish = "Finish";
        public const string Arch = "Arch";
    }
}